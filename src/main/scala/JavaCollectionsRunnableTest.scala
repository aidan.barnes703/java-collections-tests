import java.time.Instant
import java.util
import java.util.{Collections, Date, UUID}

import scala.collection.mutable.ListBuffer
import scala.util.Random
object JavaCollectionsRunnableTest extends App {

  final val TEST_SIZES = List(5, 10, 15, 25, 50, 75, 100, 1000, 10000, 100000)
  final val TEST_TYPES = List("Selector Set Insert", "Selector Set Remove", "Selector Set Contains", "Track Set Insert", "Track Set Remove", "Track Set Contains", "Track Set Head", "Track Set Time Ordered Iteration")
  final val RUN_TIMES_TO_AVERAGE = 1000
  run()

  sys.exit(0)

  def run(): Unit = {

    val headers = List("Number of Elements", "HashSet", "TreeSet", "ArrayList")

    TEST_TYPES.indices.foreach({ testType =>

      println(s"Running ${TEST_TYPES(testType)}")

      val tableBuffer: ListBuffer[List[String]] = ListBuffer()
      tableBuffer += headers

      TEST_SIZES.indices.foreach({ index =>

        val testSize = TEST_SIZES(index)

        val originalSelectorsList = generateUUIDSet(testSize)
        val originalTrackList = generateMockTracks(testSize)

        val selectorHashSet = new util.HashSet[Selector]()
        val selectorTreeSet = new util.TreeSet[Selector]()
        val selectorArrayList = new util.ArrayList[Selector]()

        originalSelectorsList.foreach(x => {
          selectorHashSet.add(x)
          selectorTreeSet.add(x)
          selectorArrayList.add(x)
        })

        val selectorToRemove = selectorArrayList.get(Random.nextInt(selectorArrayList.size()))

        val trackHashSet = new util.HashSet[Track]()
        val trackTreeSet = new util.TreeSet[Track]()
        val trackArrayList = new util.ArrayList[Track]()

        originalTrackList.foreach(x => {
          trackHashSet.add(x)
          trackTreeSet.add(x)
          trackArrayList.add(x)
        })

        val trackToRemove = trackArrayList.get(Random.nextInt(trackArrayList.size()))

        TEST_TYPES(testType) match {
          case "Selector Set Insert" => {
            tableBuffer += List(testSize.toString,
              time(selectorHashSet.add(Selector(UUID.randomUUID().toString))),
              time(selectorTreeSet.add(Selector(UUID.randomUUID().toString))),
              time(selectorArrayList.add(Selector(UUID.randomUUID().toString)))
            )
          }
          case "Selector Set Remove" => {
            tableBuffer += List(testSize.toString,
              time(selectorHashSet.remove(selectorToRemove)),
              time(selectorTreeSet.remove(selectorToRemove)),
              time(selectorArrayList.remove(selectorToRemove))
            )
          }
          case "Selector Set Contains" => {
            tableBuffer += List(testSize.toString,
              time(selectorHashSet.contains(selectorToRemove)),
              time(selectorTreeSet.contains(selectorToRemove)),
              time(selectorArrayList.contains(selectorToRemove))
            )
          }
          case "Track Set Insert" => {
            tableBuffer += List(testSize.toString,
              time(trackHashSet.add(Track(UUID.randomUUID().toString, Date.from(Instant.ofEpochMilli(System.nanoTime()))))),
              time(trackTreeSet.add(Track(UUID.randomUUID().toString, Date.from(Instant.ofEpochMilli(System.nanoTime()))))),
              time(trackArrayList.add(Track(UUID.randomUUID().toString, Date.from(Instant.ofEpochMilli(System.nanoTime())))))
            )
          }
          case "Track Set Remove" => {
            tableBuffer += List(testSize.toString,
              time(trackHashSet.remove(trackToRemove)),
              time(trackTreeSet.remove(trackToRemove)),
              time(trackArrayList.remove(trackToRemove))
            )
          }
          case "Track Set Contains" => {
            tableBuffer += List(testSize.toString,
              time(trackHashSet.contains(trackToRemove)),
              time(trackTreeSet.contains(trackToRemove)),
              time(trackArrayList.contains(trackToRemove))
            )
          }
          case "Track Set Head" => {
            tableBuffer += List(testSize.toString,
              time({
                val tmpTree = new util.TreeSet[Track]()
                tmpTree.addAll(trackHashSet)
                tmpTree.first()
              }),
              time(selectorTreeSet.first()),
              time( {
                Collections.sort(trackArrayList)
                trackArrayList.get(0)
              })
            )
          }
          case "Track Set Time Ordered Iteration" => {
            tableBuffer += List(testSize.toString,
              time({
                val tmpTree = new util.TreeSet[Track]()
                tmpTree.addAll(trackHashSet)
                val itr = tmpTree.iterator()
                while(itr.hasNext) {
                  itr.next()
                }
              }),
              time({val itr = selectorTreeSet.iterator(); while (itr.hasNext) { itr.next() }}),
              time({Collections.sort(trackArrayList)
                val itr = trackArrayList.iterator()
                while (itr.hasNext) {
                  itr.next()
                }
              })
            )
          }
        }
      })

      println(Tabulator.format(tableBuffer))
      println("=====================================================\n")


    })

  }



  def generateUUIDSet(numToGenerate: Int): List[Selector] = {
    val uuidListBuffer = ListBuffer[Selector]()
    var i = 0
    while(i < numToGenerate) {
        uuidListBuffer += Selector(UUID.randomUUID().toString)
      i += 1
    }
    uuidListBuffer.toList
  }

  def generateMockTracks(numToGenerate: Int): List[Track] ={
    val trackListBuffer = ListBuffer[Track]()
    var i = 0
    while(i < numToGenerate) {
      trackListBuffer += Track(UUID.randomUUID().toString, Date.from(Instant.ofEpochMilli(System.nanoTime())))
      i += 1
    }
    trackListBuffer.toList
  }

  def time[R](codeBlock: => R): String = {
    val t0 = System.nanoTime()
    var i = 0
    while (i < RUN_TIMES_TO_AVERAGE) {
      val result = codeBlock // call-by-name
      i += 1
    }
    val t1 = System.nanoTime()
    ((t1 - t0) / RUN_TIMES_TO_AVERAGE) + "ns"
  }
}

case class Selector(v: String) extends Comparable[Selector] {
  val key: String = v

  override def compareTo(o: Selector): Int = key.compareTo(o.key)

}


case class Track(id: String, dtg: Date) extends Comparable[Track] {

  override def compareTo(o: Track): Int = dtg.compareTo(o.dtg)

}

object Tabulator {

  def format(table: Seq[Seq[Any]]) = table match {
    case Seq() => ""
    case _ =>
      val sizes = for (row <- table) yield (for (cell <- row) yield if (cell == null) 0 else cell.toString.length)
      val colSizes = for (col <- sizes.transpose) yield col.max
      val rows = for (row <- table) yield formatRow(row, colSizes)
      formatRows(rowSeparator(colSizes), rows)
  }

  def formatRows(rowSeparator: String, rows: Seq[String]): String = (
    rowSeparator ::
      rows.head ::
      rowSeparator ::
      rows.tail.toList :::
      rowSeparator ::
      List()).mkString("\n")

  def formatRow(row: Seq[Any], colSizes: Seq[Int]) = {
    val cells = (for ((item, size) <- row.zip(colSizes)) yield if (size == 0) "" else ("%" + size + "s").format(item))
    cells.mkString("|", "|", "|")
  }

  def rowSeparator(colSizes: Seq[Int]) = colSizes map { "-" * _ } mkString("+", "+", "+")

}
